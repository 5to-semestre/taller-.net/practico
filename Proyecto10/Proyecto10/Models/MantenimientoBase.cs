﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;

namespace Proyecto10.Models
{
    public class MantenimientoBase
    {
        protected SqlConnection _conection;

        /// <summary>
        /// Conecta a base y carga conexion en atributo de clase
        /// </summary>
        protected void Conect()
        {
            string conectionString = ConfigurationManager.ConnectionStrings["BdArticulos"].ToString();
            _conection = new SqlConnection(conectionString);
        }
    }
}