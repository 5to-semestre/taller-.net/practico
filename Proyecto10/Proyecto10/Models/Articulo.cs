﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto10.Models
{
    public class Articulo
    {
        public long Id { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public float Precio { get; set; }
    }
}
