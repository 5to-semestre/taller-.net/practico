﻿using Proyecto10.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto10.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();
            var articulo = ma.Get(collection["codigo"]);

            if (articulo != null)
                return View("ModificacionArticulo", articulo);

            return View("ArticuloNoExistente");
        }

        [HttpPost]
        public ActionResult ModificacionArticulo(FormCollection collection)
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();
            Articulo articulo = new Articulo()
            {
                Id = int.Parse(collection["Id"]),
                Codigo = collection["Codigo"],
                Descripcion = collection["Descripcion"],
                Precio = float.Parse(collection["Precio"])
            };
            ma.Update(articulo);

            return RedirectToAction("Index");
        }
    }
}