﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto12.Models
{
    public class MantenimientoPersona
    {
        List<Persona> ListaPersonas = new List<Persona>()
        {
            new Persona()
            {
                Codigo = 1,
                Nombre = "Juan",
                Peso = 67.3f,
                Trabajo = false,
                FechaNacimiento = new DateTime(1970, 12, 25)
            }
        };

        public Persona Get(int Codigo)
        {
            return ListaPersonas.FirstOrDefault(p => p.Codigo == Codigo);
            //ListaPersonas resultado = null;
            //ListaPersonas.ForEach(personas =>
            //{
            //    if (persona.Codigo == codigo)
            //        resultado = persona;
            //});
            
            //foreach (var persona in ListaPersonas)
            //{
            //    if (persona.Codigo == codigo)
            //    {
            //        return persona;
            //    }
            //}
            //return null;
        }
    }
}