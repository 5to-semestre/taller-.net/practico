﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto12.Models;

namespace Proyecto12.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            MantenimientoPersona mantenimiento = new MantenimientoPersona();
            Persona persona = mantenimiento.Get(int.Parse(collection["Codigo"]));
            if (persona != null)
                return View("EditarPersona", persona);
            else
                return RedirectToAction("Index");
        }
    }
}