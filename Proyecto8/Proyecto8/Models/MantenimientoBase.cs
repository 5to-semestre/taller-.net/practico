﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Configuration;
using System.Data.SqlClient;

namespace Proyecto8.Models
{
    public class MantenimientoBase
    {
        protected SqlConnection _connection;
        /// <summary>
        /// Conecta a base y carga conexión en atributo de clase
        /// </summary>
        protected void Conect()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["BdArticulos"].ToString();
            _connection = new SqlConnection(connectionString);

        }
    }
}