﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Proyecto8.Models
{
    public class MantenimientoArticulo : MantenimientoBase
    {
        // con el summary poniendo ///, se agrega automáticamente y de este modo
        // el comentario se asocia al método y luego si dejo el mouse sobre el
        // método utilizándolo en otro lugar y ayuda a la comprensión, además
        // hay una herramienta que con esto, genera una documentación
        // automáticamente.

        /// <summary>
        /// Crea un articulo en base de datos
        /// </summary>
        /// <param name="articulo"></param>
        /// <returns></returns>
        public int Create(Articulo articulo)
        {
            Conect();
            // Si usara oracle, en vez de arroba tengo que ponerle ":".
            string sql = "insert into Articulos(Codigo, Descripcion, Precio) values (@codigo, @descripcion, @precio)";
            SqlCommand command = new SqlCommand(sql, _connection);

            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@codigo",
                SqlDbType = SqlDbType.VarChar,
                Value = articulo.Codigo
            });
            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@descripcion",
                SqlDbType = SqlDbType.VarChar,
                Value = articulo.Descripcion
            });
            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@precio",
                SqlDbType = SqlDbType.Float,
                Value = articulo.Precio
            });

            //command.Parameters.Add("@codigo", SqlDbType.VarChar);
            //command.Parameters.Add("@descripcion", SqlDbType.VarChar);
            //command.Parameters.Add("@precio", SqlDbType.Float);

            //command.Parameters["@codigo"].Value = articulo.Codigo;
            //command.Parameters["@descripcion"].Value = articulo.Descripcion;
            //command.Parameters["@precio"].Value = articulo.Precio;

            _connection.Open();
            int result = command.ExecuteNonQuery();
            _connection.Close();

            return result;
        }
        /// <summary>
        /// Retorna la lista de todos los articulos disponibles.
        /// </summary>
        /// <returns></returns>
        public List<Articulo> GetAll()
        {
            Conect();

            List<Articulo> articulos = new List<Articulo>();

            // No está bueno traer todo si no lo necesito ya que sobrecargo la conexión al cuete.
            string sql = "select * from Articulos";

            SqlCommand command = new SqlCommand(sql, _connection);

            _connection.Open();

            SqlDataReader dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                articulos.Add(new Articulo()
                {
                    Id = long.Parse(dataReader["Id"].ToString()),
                    Codigo = dataReader["Codigo"].ToString(),
                    Descripcion = dataReader["Descripcion"].ToString(),
                    Precio = float.Parse(dataReader["Precio"].ToString())
                });
            }
            _connection.Close();

            return articulos;
        }
        /// <summary>
        /// Devuelve el artículo encontrado.
        /// </summary>
        /// <param name="codigo">Código de artículo a buscar</param>
        /// <returns></returns>
        public Articulo Get(string codigo)
        {
            Conect();
            string sql = "select * from Articulos where Codigo = @codigo";
            SqlCommand command = new SqlCommand(sql, _connection);

            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@codigo",
                SqlDbType = SqlDbType.VarChar,
                Value = codigo
            });

            _connection.Open();

            SqlDataReader dataReader = command.ExecuteReader();

            Articulo articulo = new Articulo();

            if (dataReader.Read())
            {
                articulo.Id = long.Parse(dataReader["Id"].ToString());
                articulo.Codigo = dataReader["Codigo"].ToString();
                articulo.Descripcion = dataReader["Descripcion"].ToString();
                articulo.Precio = float.Parse(dataReader["Precio"].ToString());
            }
            _connection.Close();
            return articulo;
        }
        /// <summary>
        /// Devuelve el artículo encontrado.
        /// </summary>
        /// <param name="id">Id de artículo a buscar</param>
        /// <returns></returns>
        public Articulo Get(long id)
        {
            Conect();
            string sql = "select * from Articulos where Id = @Id";
            SqlCommand command = new SqlCommand(sql, _connection);

            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@Id",
                SqlDbType = SqlDbType.VarChar,
                Value = id
            });

            _connection.Open();

            SqlDataReader dataReader = command.ExecuteReader();

            Articulo articulo = new Articulo();

            if (dataReader.Read())
            {
                articulo.Id = long.Parse(dataReader["Id"].ToString());
                articulo.Codigo = dataReader["Codigo"].ToString();
                articulo.Descripcion = dataReader["Descripcion"].ToString();
                articulo.Precio = float.Parse(dataReader["Precio"].ToString());
            }
            _connection.Close();
            return articulo;
        }
        /// <summary>
        /// Actualiza los campos de un articulo.
        /// </summary>
        /// <param name="articulo">Articulo modificado, Id requerido</param>
        /// <returns></returns>
        public int Update(Articulo articulo)
        {
            Conect();
            // Si usara oracle, en vez de arroba tengo que ponerle ":".
            string sql = "update Articulos set Codigo = @codigo, Descripcion = @descripcion, Precio = @precio where Id = @id";
            SqlCommand command = new SqlCommand(sql, _connection);
            
            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@id",
                SqlDbType = SqlDbType.BigInt,
                Value = articulo.Id
            });
            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@codigo",
                SqlDbType = SqlDbType.VarChar,
                Value = articulo.Codigo
            });
            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@descripcion",
                SqlDbType = SqlDbType.VarChar,
                Value = articulo.Descripcion
            });
            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@precio",
                SqlDbType = SqlDbType.Float,
                Value = articulo.Precio
            });

            _connection.Open();
            int result = command.ExecuteNonQuery();
            _connection.Close();

            return result;

        }
        /// <summary>
        /// Elimina un artículo en Base de datos
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int Remove(long id)
        {
            Conect();
            // Si usara oracle, en vez de arroba tengo que ponerle ":".
            string sql = "delete from Articulos where Id = @id";
            SqlCommand command = new SqlCommand(sql, _connection);

            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@id",
                SqlDbType = SqlDbType.BigInt,
                Value = id
            });

            _connection.Open();
            int result = command.ExecuteNonQuery();
            _connection.Close();

            return result;

        }
    }
}
