﻿using Proyecto8.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Proyecto8.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();
            return View(ma.GetAll());
        }
        public ActionResult Alta()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Alta(FormCollection collection)
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();
            Articulo articulo = new Articulo()
            {
                Codigo = collection["codigo"],
                Descripcion = collection["descripcion"],
                Precio = float.Parse(collection["precio"])
            };
            ma.Create(articulo);
            return RedirectToAction("Index");
        }
        public ActionResult Baja(long id)
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();
            ma.Remove(id);
            return RedirectToAction("Index");
        }
        public ActionResult Modificacion(long id)
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();
            var articulo = ma.Get(id);
            return View(articulo);
        }
        [HttpPost]
        public ActionResult Modificacion(FormCollection collection)
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();
            Articulo articulo = new Articulo()
            {
                Id = long.Parse(collection["id"]),
                Codigo = collection["codigo"],
                Descripcion = collection["descripcion"],
                Precio = float.Parse(collection["precio"])
            };
            ma.Update(articulo);
            return RedirectToAction("Index");
        }
    }
}