﻿using Proyecto9.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto8.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();
            return View(ma.GetAll());
        }

        // GET: Home/Details/5
        public ActionResult Details(int id)
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();
            Articulo articulo = ma.Get(id);
            return View(articulo);
        }

        // GET: Home/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Home/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                MantenimientoArticulo ma = new MantenimientoArticulo();
                Articulo articulo = new Articulo()
                {
                    Codigo = collection["codigo"],
                    Descripcion = collection["descripcion"],
                    Precio = float.Parse(collection["precio"].ToString())
                };
                ma.Create(articulo);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Home/Edit/5
        public ActionResult Edit(int id)
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();
            Articulo articulo = ma.Get(id);
            return View(articulo);
        }

        // POST: Home/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                MantenimientoArticulo ma = new MantenimientoArticulo();
                Articulo articulo = new Articulo()
                {
                    Id = id,
                    Codigo = collection["codigo"],
                    Descripcion = collection["descripcion"],
                    Precio = float.Parse(collection["precio"].ToString())
                };

                ma.Update(articulo);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Home/Delete/5
        public ActionResult Delete(int id)
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();
            Articulo articulo = ma.Get(id);

            return View(articulo);
        }

        // POST: Home/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                MantenimientoArticulo ma = new MantenimientoArticulo();
                ma.Remove(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
