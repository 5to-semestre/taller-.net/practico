﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Proyecto9.Models
{
    public class MantenimientoArticulo : MantenimientoBase
    {

        /// <summary>
        /// Crea un articulo en base de datos
        /// </summary>
        /// <param name="articulo"></param>
        /// <returns></returns>
        public int Create(Articulo articulo)
        {

            Conect();

            string sql = "insert into Articulos(Codigo, Descripcion, Precio)"
                      + " values (@codigo, @descripcion, @precio)";

            SqlCommand command = new SqlCommand(sql, _conection);

            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@codigo",
                SqlDbType = SqlDbType.VarChar,
                Value = articulo.Codigo
            });
            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@descripcion",
                SqlDbType = SqlDbType.VarChar,
                Value = articulo.Descripcion
            });
            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@precio",
                SqlDbType = SqlDbType.Float,
                Value = articulo.Precio
            });

            //command.Parameters.Add("@codigo", SqlDbType.VarChar);
            //command.Parameters.Add("@descripcion", SqlDbType.VarChar);
            //command.Parameters.Add("@precio", SqlDbType.Float);

            //command.Parameters["@codigo"].Value = articulo.Codigo;
            //command.Parameters["@descripcion"].Value = articulo.Descripcion;
            //command.Parameters["@precio"].Value = articulo.Precio;

            _conection.Open();

            int result = command.ExecuteNonQuery();

            _conection.Close();

            return result;

        }

        /// <summary>
        /// Retorna la lista de todos los articulos disponibles
        /// </summary>
        /// <returns></returns>
        public List<Articulo> GetAll()
        {
            Conect();

            List<Articulo> articulos = new List<Articulo>();

            string sql = "select * from Articulos";

            SqlCommand command = new SqlCommand(sql, _conection);

            _conection.Open();

            SqlDataReader dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                articulos.Add(new Articulo()
                {
                    Id = long.Parse(dataReader["Id"].ToString()),
                    Codigo = dataReader["Codigo"].ToString(),
                    Descripcion = dataReader["Descripcion"].ToString(),
                    Precio = float.Parse(dataReader["Precio"].ToString())
                });
            }

            _conection.Close();

            return articulos;
        }

        /// <summary>
        /// Devuelve el articulo encontrado
        /// </summary>
        /// <param name="codigo"> Codigo de articulo a buscar</param>
        /// <returns></returns>
        public Articulo Get(string codigo)
        {
            Conect();

            string sql = "select * from Articulos where Codigo = @codigo";
            SqlCommand command = new SqlCommand(sql, _conection);

            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@codigo",
                SqlDbType = SqlDbType.VarChar,
                Value = codigo
            });

            _conection.Open();

            SqlDataReader dataReader = command.ExecuteReader();

            Articulo articulo = new Articulo();

            if (dataReader.Read())
            {
                articulo.Id = long.Parse(dataReader["Id"].ToString());
                articulo.Codigo = dataReader["Codigo"].ToString();
                articulo.Descripcion = dataReader["Descripcion"].ToString();
                articulo.Precio = float.Parse(dataReader["Precio"].ToString());
            }

            _conection.Close();

            return articulo;

        }
        /// <summary>
        /// Devuelve el articulo encontrado
        /// </summary>
        /// <param name="id"> Id de articulo a buscar</param>
        /// <returns></returns>
        public Articulo Get(long id)
        {
            Conect();

            string sql = "select * from Articulos where Id = @id";
            SqlCommand command = new SqlCommand(sql, _conection);

            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@id",
                SqlDbType = SqlDbType.BigInt,
                Value = id
            });

            _conection.Open();

            SqlDataReader dataReader = command.ExecuteReader();

            Articulo articulo = new Articulo();

            if (dataReader.Read())
            {
                articulo.Id = long.Parse(dataReader["Id"].ToString());
                articulo.Codigo = dataReader["Codigo"].ToString();
                articulo.Descripcion = dataReader["Descripcion"].ToString();
                articulo.Precio = float.Parse(dataReader["Precio"].ToString());
            }

            _conection.Close();

            return articulo;

        }

        /// <summary>
        /// Actualiza los campos de un articulo
        /// </summary>
        /// <param name="articulo"> Articulo modificado, Id requerido</param>
        /// <returns></returns>
        public int Update(Articulo articulo)
        {
            Conect();

            string sql = "update Articulos set Codigo = @codigo, Descripcion = @descripcion, Precio = @precio "
                      + "where Id = @id";

            SqlCommand command = new SqlCommand(sql, _conection);

            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@id",
                SqlDbType = SqlDbType.BigInt,
                Value = articulo.Id
            });
            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@codigo",
                SqlDbType = SqlDbType.VarChar,
                Value = articulo.Codigo
            });
            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@descripcion",
                SqlDbType = SqlDbType.VarChar,
                Value = articulo.Descripcion
            });
            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@precio",
                SqlDbType = SqlDbType.Float,
                Value = articulo.Precio
            });

            _conection.Open();

            int result = command.ExecuteNonQuery();

            _conection.Close();

            return result;

        }

        /// <summary>
        /// Elimina un articulo en base de datos
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int Remove(long id)
        {
            Conect();

            string sql = "delete from Articulos where Id = @id";

            SqlCommand command = new SqlCommand(sql, _conection);

            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@id",
                SqlDbType = SqlDbType.BigInt,
                Value = id
            });

            _conection.Open();

            int result = command.ExecuteNonQuery();

            _conection.Close();

            return result;

        }


    }
}