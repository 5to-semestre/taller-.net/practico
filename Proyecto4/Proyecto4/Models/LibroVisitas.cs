﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Hosting;
using System.Configuration;

namespace Proyecto4.Models
{
    public class LibroVisitas
    {
        private string _direccionArchivo;
        public LibroVisitas()
        {
            _direccionArchivo = HostingEnvironment.MapPath("~") + "/App_Data/datos.txt";
        }
        public void Grabar (string nombre, string comentarios)
        {
            //Obtiene el archivo, si no existe lo crea, y anexa el comentario nuevo
            StreamWriter archivo = new StreamWriter(_direccionArchivo, true);

            //------------------------------------------------------------------------------------------------
            //Obtengo la dirección desde el archivo de configuración de mi aplicación, esta es una forma.
            //string direccion = ConfigurationManager.AppSettings("DIR.ARCHIVO");
            //StreamWriter archivo = new StreamWriter($"{direccion}/datos.txt", true);
            //------------------------------------------------------------------------------------------------

            archivo.WriteLine($"Nombre: {nombre} <br>Comentario: {comentarios}<hr>");
            //Cuando concatenas con + consume más recursos, así que hacerlo de esta manera
            //es una bunea práctica.
            archivo.Close();
            //Se cierra para que no me dé problemas en el futuro porque si no me queda abiero en el infinito lel.
        }
        public string leer()
        {
            StreamReader archivo = new StreamReader(_direccionArchivo, true);
            string todo = archivo.ReadToEnd();
            archivo.Close();
            return todo;
        }
    }
}