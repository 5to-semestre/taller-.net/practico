﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto4.Models;//No me dejó importar la clase concreta.

namespace Proyecto4.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult FormularioVisita()
        {
            return View();
        }
        public ActionResult CargarDatos()
        {
            string nombre = Request.Form["nombre"].ToString();
            string comentarios = Request.Form["comentarios"].ToString();
            LibroVisitas libro = new LibroVisitas();
            libro.Grabar(nombre, comentarios);
            //Retorno a vista Index
            //return View("Index");
            
            //Retornar a vista correspondiente al metodo CargarDatos.
            return View("Index");
        }
        public ActionResult ListadoVisitas()
        {
            LibroVisitas libro = new LibroVisitas();
            string todo = libro.leer();
            //Diccionario para enviar info a la vista.
            ViewData["libro"] = todo;
            return View();
        }
    }
}